var nameArray = [];

d3.json("data/sample.json", function (data) {
    passData(data);
});

function passData(inputData) {
    var c = groupJson(inputData);
    createGraph(c);
}



function groupJson(edges) {
    var sas = [];
    var m = {};
    var fieldA = '';
    var fieldB = '';

    m.name = "Indian MP List with Winning Percentage";
    m.children = [];
    for (var key in edges) {
        var l = {};
        l.name = key;
        l.children = [];
        for (var i = 0; i < edges[key].length; i++) {
            var k = {};
            k.name = edges[key][i]["MP"];
            k.size = edges[key][i]["Percentage"];
            l.children.push(k);
        }
        m.children.push(l);
    }
    console.log(m)
    return m;
}